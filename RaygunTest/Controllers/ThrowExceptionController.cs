﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace RaygunTest.Controllers
{
    public class ThrowExceptionController : ApiController
    {
        public void Get()
        {
            throw new Exception("This was an exception thrown from a Web API controller to test Raygun.io");
        }
	}
}