﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mindscape.Raygun4Net.WebApi;

namespace RaygunTest.Controllers
{
    public class CustomExceptionController: ApiController
    {
        public string Get()
        {
            var client = new RaygunWebApiClient();
            var exception = new Exception("This is an exception sent in the background to test Raygun.io");
            client.Send(exception);
            return "some text";
        }
    }
}