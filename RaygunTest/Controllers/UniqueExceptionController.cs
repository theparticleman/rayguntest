﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mindscape.Raygun4Net.WebApi;

namespace RaygunTest.Controllers
{
    public class UniqueExceptionController : ApiController
    {
        public string Get()
        {
            var guid = Guid.NewGuid();
            new RaygunWebApiClient().SendInBackground(new Exception("This is an exception with a unique message " + guid));
            return "Exception sent in background: " + guid;
        }
    }
}
